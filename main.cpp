//Name: Brody Paulk
//Assignment: Lab 4
//Section: Section 4
#include "iostream"
#include "string"
#include "cstdlib"
#include <chrono>
#include "algorithm"

using namespace std::chrono;
using namespace std;

void bubbleSort(int arr[], int size);

void selectionSort(int arr[], int size);

void insertionSort(int arr[], int size);

void countSort(int arr[], int size);

void swap(int &i, int &j);

void printArray(int arr[], int size);

void printDuration(const high_resolution_clock::time_point &start, const high_resolution_clock::time_point &stop);

void fillRandomArray(int arr[], int size);
int main() {
    srand(time(nullptr));

    int size = 5000;
    int unsortedArray[size], toSort[size];

    fillRandomArray(unsortedArray, size);
    // printArray(unsortedArray, size);
    // Before function call
    printf("Bubble Sort\n");
    // make copy of unsorted array
    copy_n(unsortedArray, size,toSort);
    high_resolution_clock::time_point start = high_resolution_clock::now();
    bubbleSort(toSort, size);
    // After function call
    high_resolution_clock::time_point stop = high_resolution_clock::now();
    printDuration(start, stop);

    printf("Selection Sort\n");
    copy_n(unsortedArray, size,toSort);
    start = high_resolution_clock::now();
    selectionSort(toSort, size);
    stop = high_resolution_clock::now();
    printDuration(start, stop);

    
    printf("Insertion Sort\n");
    copy_n(unsortedArray, size,toSort);
    start = high_resolution_clock::now();
    insertionSort(toSort, size);
    stop = high_resolution_clock::now();
    printDuration(start, stop);

    printf("Count Sort\n");
    copy_n(unsortedArray, size,toSort);
    start = high_resolution_clock::now();
    countSort(toSort, size);
    stop = high_resolution_clock::now();
    printDuration(start, stop);

    return 0;
}

void fillRandomArray(int arr[], int size) {
for (int i = 0; i < size; i++) {
        arr[i] = rand() % 100000;
    }
}

void printDuration(const high_resolution_clock::time_point &start, const high_resolution_clock::time_point &stop) {// Subtract stop and start timepoints and
    // cast it to required unit. Predefined units
    // are nanoseconds, microseconds, milliseconds,
    // seconds, minutes, hours. Use duration_cast()
    // function.
    // auto duration = duration_cast<nanoseconds>(stop - start);

    // To get the value of duration use the count()
    // member function on the duration object
    printf("Duration: %lld%s\n", duration_cast<nanoseconds>(stop - start).count(), "ns");
    printf("Duration: %lld%s\n", duration_cast<microseconds>(stop - start).count(), "µs");
    printf("Duration: %lld%s\n", duration_cast<milliseconds>(stop - start).count(), "ms");
}

void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void swap(int &i, int &j) {
    int temp = i;
    i = j;
    j = temp;
}
// Compares a value to the value behind it, and moves it accordingly
void bubbleSort(int arr[], int size) {
    int i, j;
// Establishes boundary based on "size"
for(i = 0; i<10; i++) {
   // Compares current value to the value previous
   for(j = i+1; j<size; j++) {
        if(arr[j] < arr[i]) {
        // moves the value of the element to the element behind it
         swap(arr[j],arr[i]);
      }
   }
}

}
// Finds the minimum value and moves it to the beginning, then restricts the boundary by one
void selectionSort(int arr[], int size) {
    int i, j, minval;
 
    // Moves boundary over one
    for (i = 0; i < size-1; i++)
    {
        // Find the minimum value
        minval = i;
        for (j = i+1; j < size; j++)
        if (arr[j] < arr[minval])
            minval = j;
 
        // moves the minimum value to the beginning boundary
         swap(arr[minval], arr[i]);
    }
}
//Moves actual elements based on value
void insertionSort(int arr[], int size) {
        int i, comparison, j;
    for (i = 1; i < size; i++)
    {
        comparison = arr[i];
        j = i - 1;

        // Move all elements prior to current up one position
        while (j >= 0 && arr[j] > comparison)
        {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = comparison;
    }
    
}
//calculates frequency of a value to sort
void countSort(int arr[], int size) {
  int max = arr[0];
  int output[size], count[max+1];
    
    //finds the max value of input array
    for (int i = 0; i < size-1; i++) {
        if (arr[i] > max)
            max = arr[i];
    }
    //start count array at 0
    for (int i = 0; i <= max; ++i) {
    count[i] = 0;
    }
    //add 1 to frequency of value
    for (int i = 0; i < size-1; i++) {
      count[arr[i]]++;
    }
    //store the total count of the arrays
    for (int i = 1; i <= max; i++) {
    count[i] = count[i] + count[i - 1];
    }
    //place the elements in output array
    for (int i = arr[size] - 1; i >= 0; i--) {
      output[count[arr[i]] - 1] = arr[i];
      count[arr[i]]--;
    }
    //copy the sorted elements to original array
    for (int i = 0; i < arr[size]; i++) {
      arr[i] = output[i];
    }
}